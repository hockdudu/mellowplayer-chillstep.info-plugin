function update() {
    let status;
    if (h5playa.readyState === 0) {
        status = MellowPlayer.PlaybackStatus.STOPPED;
    } else if (h5playa.readyState <= 2) {
        status = MellowPlayer.PlaybackStatus.BUFFERING;
    } else {
        status = MellowPlayer.PlaybackStatus.PLAYING;
    }

    const volume = h5playa.volume;

    let author = '';
    let title = convertHTMLEntity(titleRequest.previousTitle ?? '');
    const songId = getHashCode(title);

    if (title.includes(' - ')) {
        [author, ...title] = title.split(' - ');
        title = title.join(' - ');
    }

    return {
        "playbackStatus": status,
        "canSeek": false,
        "canGoNext": false,
        "canGoPrevious": false,
        "canAddToFavorites": false,
        "volume": volume,
        "duration": 0,
        "position": 0,
        "songId": songId,
        "songTitle": title,
        "artistName": author,
        "albumTitle": '',
        "artUrl": '',
        "isFavorite": false
    };
}

function play() {
    $(window).fireEvent('userplay');
}

function pause() {
    $(window).fireEvent('userstop');
}

function goNext() {
    // noop
}

function goPrevious() {
    // noop
}

function setVolume(volume) {
    const slider = document.getElementById('volume-container');
    const knob = document.getElementById('volume-knob');

    const sliderRect = slider.getBoundingClientRect();
    const knobRect = knob.getBoundingClientRect();
    const halfKnob = knobRect.width / 2;

    const clientX = sliderRect.x + halfKnob + (sliderRect.width - knobRect.width) * volume;
    const clientY = sliderRect.y;

    const event = new MouseEvent('mousedown', {
        clientX: clientX,
        clientY: clientY,
        screenX: clientX,
        screenY: clientY,
    });

    slider.dispatchEvent(event);
}

function addToFavorites() {
    // noop
}

function removeFromFavorites() {
    // noop
}

function seekToPosition(position) {
    // noop
}

function convertHTMLEntity(text) {
    const span = document.createElement('span');

    return text.replace(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
        span.innerHTML = entity;
        return span.innerText;
    });
}
